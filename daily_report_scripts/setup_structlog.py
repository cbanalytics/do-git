import inspect
import socket
import logging
import structlog
import sys
from logging.handlers import RotatingFileHandler


def init_app(app, filename):
    logger = logging.getLogger(app)
    logging.basicConfig(
        filename=filename,
        format="%(message)s",
        level=logging.INFO,
    )

    fh = RotatingFileHandler(
        filename, mode='a', maxBytes=10240000, backupCount=5, encoding=None)
    #formatter = logging.Formatter('%(asctime)s %(pathname)s %(process)d %(message)s')
    # fh.setFormatter(formatter)
    logger.addHandler(fh)

    structlog.configure(
        processors=[
            structlog.stdlib.filter_by_level,
            structlog.stdlib.add_logger_name,
            structlog.stdlib.add_log_level,
            structlog.stdlib.filter_by_level,
            _add_server_info,
            _add_caller_info,
            structlog.stdlib.PositionalArgumentsFormatter(True),
            structlog.processors.TimeStamper(fmt="%Y-%m-%d %H:%M:%S"),
            structlog.processors.StackInfoRenderer(),
            structlog.processors.format_exc_info,
            structlog.processors.UnicodeDecoder(),
            structlog.processors.JSONRenderer(),
        ],
        context_class=structlog.threadlocal.wrap_dict(dict),
        logger_factory=structlog.stdlib.LoggerFactory(),
        cache_logger_on_first_use=True,
    )


def getLog():
    log = structlog.get_logger()
    return log


def _add_caller_info(logger, method_name, event_dict):
    frame = inspect.currentframe()
    while frame:
        frame = frame.f_back
        module = frame.f_globals['__name__']
        if module.startswith('structlog.'):
            continue
        event_dict['filename'] = frame.f_code.co_filename
        event_dict['func'] = frame.f_code.co_name
        event_dict['lineno'] = frame.f_lineno
        return event_dict


def _add_server_info(logger, method_name, event_dict):
    event_dict['servername'] = socket.gethostname()
    event_dict['serverip'] = get_ip()
    return event_dict


def get_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        # doesn't even have to be reachable
        s.connect(('10.255.255.255', 1))
        IP = s.getsockname()[0]
    except:
        IP = '127.0.0.1'
    finally:
        s.close()
    return IP


def get_request_details(request):
    requestDetails = dict()
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    requestDetails["user_agent"] = request.META.get(
        "HTTP_USER_AGENT", "UNKNOWN")
    if x_forwarded_for:
        requestDetails["client_ip"] = x_forwarded_for.split(',')[0]
    else:
        requestDetails["client_ip"] = request.META.get('REMOTE_ADDR')
    return requestDetails
