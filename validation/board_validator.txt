db.runCommand({'collMod' : 'board',
   validator: { 
	$expr : {
		"$and" : [
			{  "$lt": [ { "$strLenCP": "$board_description.en" }, 400 ]  },
			{  "$lt": [ { "$strLenCP": "$board_name.en" }, 200 ]  },
			 { "board_description" : {"en" : { bsonType: "string" } } },
			 { "board_name" : {"en" : { bsonType: "string" } } },
	]},
$jsonSchema: {
      bsonType: "object",
      additionalProperties: false,
      required: [ "board_card_count", "board_cat", "board_charm_count", "board_description", "board_id", "board_name", "board_open_option", "board_type", "board_video_count", "created_on", "deleted", "lang", "user_id", ],
      properties: {
	"_id": {},
	"bimg": { bsonType: "string", },
	"board_card_count": { bsonType: "int", },
	"board_cat": { enum: [ "jewellery", "footwear", "wfashion", "hairbeauty", "makeup", "Clothes", "actors", "", "mfashion", "character", "travel", "video", "weddings", "facc", "mensgrooming", "topcharms", "beauty", "bags" ], },
	"board_charm_count": { bsonType: "int", },
	"board_description": { bsonType: "object", 
			properties: {
				"en": { bsonType: "string", },
				"en-gb": { bsonType: "string", },
			},}, 
	"board_id": { bsonType: "int", }, 
	"board_name": { bsonType: "object", 
			properties: {
				"en": { bsonType: "string", },
				"en-gb": { bsonType: "string", },
			},}, 
	"board_open_option": { enum: ["charms", "cards"], },
	"board_ranking": { bsonType: "int", },
	"board_type": { enum : [ "custom", "default", "system" ], },
	"board_video_count": { bsonType: "int", },
	"board_vis": { bsonType: "int", },
	"created_on": { bsonType: ["long", "double",], },
	"deleted": { enum: [ NumberInt(0), NumberInt(1) ], },
	"lang": { bsonType: "array", items: { bsonType: "string", },}, 
	"modified_on": { bsonType: ["long", "double", "int"], },
	"user_id": { bsonType: "number", }, 
      }
   } },
"validationLevel" : "strict",
"validationAction" : "error"
} );
