from pymongo import MongoClient
from bson.objectid import ObjectId
import configparser
config = configparser.ConfigParser()
config.read('/home/swetha/config.ini')

#uri = config['mongo']['mongouri_read']
#uri = "mongodb://tagosprod:tagosprod#132@ds119295-a0.mlab.com:19295/charmboard"
#uri = "mongodb://cbappdev:cbdevap#132@cb-app-0.charmd.me:19295/charmboard_dev"
uri = "mongodb://139.59.83.215:27017/charmboard_dev" #change
client = MongoClient(uri)
db = client.get_database()
#db = client.charmboard_20180607 #change
#coll = db.charm_noval
coll = db.charm

required_list = [ "aux_charm_flag", "charm_id", "charm_like_count", "charm_save_count", "charm_touch_count", "charm_view_count", "chsketch_id", "created_on", "live_status", "look", "modified_on", "objects_in_charm", "subtitle", "tagger_object", "title", ]

allowed_list = ["_id", "actor_id", "associated_videos", "aux_charm_flag", "board_obj", "character_id", "charm_id", "charm_like_count", "charm_save_count", "charm_touch_count", "charm_view_count", "chsketch_id", "content_image_cards", "created_on", "ext_likes", "fscore", "isGif", "lang", "lc_bounding_box", "live_status", "look", "modified_on", "objects_in_charm", "production_name", "qscore", "ref_cards", "search_desc", "search_keywords", "subtitle", "tagger_object", "title", "video_id",  ]

for charm in coll.find({"charm_id" : {"$in" : [331690]}}): #{ "_id" : { "$gt" : ObjectId("5b129e551f6fda4376a70762") }}):
#	print(charm['charm_id'])
	keys_list = charm.keys()
	for req_field in required_list:
		if req_field not in keys_list:
			print(charm['charm_id'],req_field,"missing")
	for key in keys_list:
		if key not in allowed_list:
			print(charm['charm_id'],key,"is not allowed")
		else:
			if key == "_id":
				continue
			elif key in ["lc_bounding_box", "production_name", "tagger_object", "video_id"]:
				if type(charm[key]).__name__ != "str":
					print(charm['charm_id'],type(charm[key]).__name__,key,"datatype mismatch")
			elif key in ['charm_id','charm_like_count','charm_save_count','charm_touch_count','charm_view_count']:
				if type(charm[key]).__name__ != 'int':
					print(charm['charm_id'],type(charm[key]).__name__,key,"datatype mismatch")
			elif key in ["aux_charm_flag", "live_status", "isGif", "look"]:
				if type(charm[key]).__name__ != 'int':
					print(charm['charm_id'],type(charm[key]).__name__,key,"datatype mismatch")
				elif charm[key] not in [0,1]:
					print(charm['charm_id'],type(charm[key]).__name__,key,"unacceptable value")
			elif key in ["created_on", "modified_on", ]:
				if type(charm[key]).__name__ not in ["Int64", "float"]:
					print(charm['charm_id'],type(charm[key]).__name__,key,"datatype mismatch")
			elif key in ["actor_id", "character_id", "chsketch_id", "fscore" , "qscore" ]:
				if type(charm[key]).__name__ not in ["int", "float"]:
					print(charm['charm_id'],type(charm[key]).__name__,key,"datatype mismatch")
			elif key in ["ext_likes", "lang", "objects_in_charm", "search_desc", "search_keywords"]:
				if type(charm[key]).__name__ != "list":
					print(charm['charm_id'],type(charm[key]).__name__,key,"datatype mismatch")
			elif key in ['title','subtitle',]:
				inner_keys = charm[key].keys()
				for inner_key in inner_keys:
					if inner_key not in ['en','en-gb']:
						print(charm['charm_id'],key,repr(inner_key),"not allowed")
					elif type(charm[key][inner_key]).__name__ != 'str':
						print(charm['charm_id'],key,inner_key,type(video[inner_key]).__name__,"datatype mismatch")
			elif key == 'associated_videos':
				videos = charm[key]
				try:
					for video in videos:
						inner_keys = video.keys()
						reqd_inner_keys = [ "video_url", "video_id", "video_type", "video_category", "video_year", "video_name",]
						for reqd_inner_key in reqd_inner_keys:
							if reqd_inner_key not in inner_keys:
								print(charm['charm_id'],inner_key,"missing")
						for inner_key in inner_keys:
							if inner_key not in ['band_name', 'cueid', 'movie_name', 'video_url', 'song_name', 'video_id', 'video_type', 'video_category', 'video_year', 'video_name',]:
								print(charm['charm_id'],key,repr(inner_key),"not allowed")
							else:
								if inner_key in ['band_name','movie_name','video_url','song_name','video_id','video_name',]:
									if type(video[inner_key]).__name__ != 'str':
										print(charm['charm_id'],key,inner_key,type(video[inner_key]).__name__,"datatype mismatch")
								elif inner_key in ['cueid','video_year']:
									if type(video[inner_key]).__name__ not in ['int','float',]:
										print(charm['charm_id'],key,inner_key,type(video[inner_key]).__name__,video[inner_key], "datatype mismatch")
								elif inner_key == 'video_type':
									if video[inner_key] not in [ "Movie", "Television/Serial", "Ads", "Web Series" , "Others", ]:
										print(charm['charm_id'],key,inner_key,video[inner_key],"incorrect value")
								elif inner_key == 'video_category':
									if video[inner_key] not in [ "Song", "Trailer", "Episode", "Web Series", "Ads", "Talk Show", "Song - Movie", "Song - Single", "Song - Album", "News","Food","Technology","Sports","Short Film","Travel","Brand Photo Shoot","Magazine Video", "Event", "Fitness", "Others", "Tutorial", ]:
										print(charm['charm_id'],key,inner_key,video[inner_key],"incorrect value")
				except TypeError:
					print(charm['charm_id'],key,"empty list", charm[key])
			elif key == 'board_obj':
				videos = charm[key]
				for video in videos:
					inner_keys = video.keys()
					for inner_key in inner_keys:
						if inner_key not in ["board_id", "video_id"]:
							print(charm['charm_id'],inner_key,"missing")
							continue
						if inner_key not in ["board_id", "video_id"]:
							print(charm['charm_id'],key,repr(inner_key),"not allowed")
						else:
							if inner_key in ['video_id',]:
								if type(video[inner_key]).__name__ != 'str':
									print(charm['charm_id'],key,inner_key,type(video[inner_key]).__name__,"datatype mismatch")
							elif inner_key in ['board_id']:
								if type(video[inner_key]).__name__ not in ['int',]:
									print(charm['charm_id'],key,inner_key,type(video[inner_key]).__name__,"datatype mismatch")
			elif key == 'content_image_cards':
				videos = charm[key]
				try:
					for video in videos:
						inner_keys = video.keys()
						for inner_key in inner_keys:
							if inner_key not in ["card_color", "card_id",'dis_card_type', 'card_rank', 'cb_category', 'sub_category', 'subsub_category', 'text',]:
								print(charm['charm_id'],key,repr(inner_key),"not allowed")
							else:
								if inner_key in ['dis_card_type', 'text',]:
									if type(video[inner_key]).__name__ != 'str':
										print(charm['charm_id'],key,inner_key,type(video[inner_key]).__name__,"datatype mismatch")
								elif inner_key in ['card_id', ]:
									if type(video[inner_key]).__name__ not in ['int','str', 'float']:
										print(charm['charm_id'],key,inner_key,type(video[inner_key]).__name__,"datatype mismatch")
								elif inner_key in ['card_id', 'card_rank']:
									if type(video[inner_key]).__name__ not in ['int','str']:
										print(charm['charm_id'],key,inner_key,type(video[inner_key]).__name__,"datatype mismatch")
								elif inner_key == 'card_color':
									if video[inner_key] not in ["assorted", "beige", "beige pink", "beige purple", "black", "blue", "bronze", "brown", "burgundy", "camel", "camel brown", "champagne", "charcoal", "coffee brown", "cognac", "copper", "coral", "cream", "fluorescent", "fluorescent green", "fuchsia", "gold", "gold silver", "green", "grey", "grey melange", "gunmetal", "khaki", "lavender", "lime green", "magenta", "maroon", "mauve", "melange", "metallic", "multi", "mushroom", "mushroom brown", "mustard", "navy", "navy blue", "nude", "off white", "olive", "orange", "peach", "peach cream", "pink", "purple", "red", "red grey", "rose", "rose gold", "rust", "sea green", "silver", "skin", "steel", "tan", "taupe", "teal", "transparent", "turquoise", "violet", "white", "yellow", "others", "" ]: #"light", "ligth", "-1", "brown family", "white family", "light blue", "light brown", "light orange", "none", "ligth brown", None, "" ]:
										print(charm['charm_id'],key,inner_key,video[inner_key],"incorrect value")
								elif inner_key == 'cb_category':
									if video[inner_key] not in [ "outfit", "accessories", "beauty", "hair", "lingerie", "", "default" ]:
										print(charm['charm_id'],key,inner_key,video[inner_key],"incorrect value")
								elif inner_key == 'sub_category':
									if video[inner_key] not in ["bags and wallets", "bottomwear", "default", "eyes", "eyewear", "face", "fashion accessories", "footwear", "hair care", "hair styling", "innerwear", "jewellery", "lips", "makeup tools", "nails", "others", "stoles and scarves", "sunglasses", "topwear", "top and bottom", "" ]:
										print(charm['charm_id'],key,inner_key,video[inner_key],"incorrect value")
								elif inner_key == 'subsub_category':
									if video[inner_key] not in ["anarkali", "anklet", "aviator sunglasses", "baby doll", "backpack", "bag", "bajuband", "bali", "ballerina", "bangles", "belt", "bikini", "blazer", "blouse", "blush", "boat shoes", "body chain", "bodysuit", "boots", "bra", "bracelet", "briefs", "bronzer", "brooch", "brush", "butterfly sunglasses", "camisole", "canvas shoes", "cap", "capris", "cardigan", "cargo", "casual shoes", "cat eye sunglasses", "chain", "chinos", "churha", "churidar", "clay", "clubmaster sunglasses", "clutch", "coat", "concealer", "conditioner", "contour", "cream", "cufflinks", "dangler", "denim", "derby", "dhoti", "dhoti pants", "dhoti salwar", "dress", "duffle", "dungarees", "dupatta", "earrings", "eye shadow", "eyebrow pencil", "eyeliner", "face wash", "facemask", "facial", "flats", "flip flops", "floaters", "formal shoes", "foundation", "frames", "gel", "garter belts", "gloves", "gown", "hair appliance", "hair brush", "hair color", "hair comb", "hair cream", "hair heat protector", "hair oil", "hair styler", "hairband", "hairband or head scarf", "hand harness", "hand thong", "handbag", "hat", "heels", "highlighter", "hoodie", "jacket", "jeans", "jeggings", "jewellery set", "jhumki", "joggers", "jumpsuit", "jutti or mojari", "kada", "kajal", "kamarband or belly chain", "kimono", "kurta", "kurta salwar", "kurta-dhoti set", "kurta-pyjama set", "kurti", "lapel pins", "lashes", "leggings", "lehenga", "lingerie", "lip balm", "lip gloss", "lipstick", "loafer", "locket", "loungewear", "maangtika", "makeuppallette", "mangalsutra", "mascara", "masque or masks", "moccasin", "moisturizer", "mongo db entry", "mousse", "nail gloss", "nail paint", "necklace", "nightdress", "nightsuit", "non product", "nose ring or pin", "otherbags", "otherbottomwear", "othereyeproducts", "othereyewear", "otherfaceproducts", "otherfashionaccessories", "otherfootwear", "otherhairproducts", "otherhairstylingproducts", "otherinnerwear", "otherjewellery", "otherlipsproduct", "othernailproducts", "others", "otherstoleandscarves", "othersunglasses", "othertopwear", "outdoor shoes", "oval sunglasses", "palazzo pants", "palazzo suit", "panties", "pants", "panty hose", "pathani suit", "patiala pants", "patiala suit", "pendant", "platform", "pocket squares", "pomade", "powder", "primer", "pump", "pyjamas", "rectangle sunglasses", "ring", "robe", "round sunglasses", "safa", "salwar", "salwar and dupatta", "salwar suit", "sandals", "saree", "saree gown", "scarf", "semi rimless sunglasses", "serum", "shampoo", "shapewear", "sharara", "shawl", "sherwani", "sherwani dhoti set", "shield sunglasses", "shirt", "shorts", "shrug", "skirts", "slip-ons", "slippers", "sneakers", "snood", "socks", "spectacles", "sponge", "sports shoes", "spray", "square sunglasses", "stiletto", "stockings", "stole", "stud", "suits", "sunglasses", "suspenders", "sweaters", "sweatshirts", "swimsuit", "t-shirt", "tie", "tights", "tops", "trackpants", "tracksuit", "trainers", "treggings", "trouser", "tunic", "turbans", "vest", "waistcoat", "wallet", "watch", "wax", "wayfarer sunglasses", "wedges", "", "default"]:
										print(charm['charm_id'],key,inner_key,video[inner_key],"incorrect value")
				except TypeError:
					#continue
					print(charm['charm_id'],key,"is null",charm[key])
			elif key == 'ref_cards':
				videos = charm[key]
				for video in videos:
					inner_keys = video.keys()
					for inner_key in inner_keys:
						if inner_key not in ["card_id", "category",'product_url','title','type']:
							print(charm['charm_id'],key,repr(inner_key),"not allowed")
						else:
							if inner_key in ["category",'product_url','title','type']:
								if type(video[inner_key]).__name__ != 'str':
									print(charm['charm_id'],key,inner_key,type(video[inner_key]).__name__,"datatype mismatch")
							elif inner_key in ['card_id']:
								if type(video[inner_key]).__name__ not in ['int','float', ]:
									print(charm['charm_id'],key,inner_key,type(video[inner_key]).__name__,"datatype mismatch")
			#elif key == "additional_attributes":
			#	if type(charm[key]).__name__ != 'dict':
			#		print(charm['charm_id'],type(charm[key]).__name__,key,"datatype mismatch")
			else:
				print(charm['charm_id'],key,type(charm[key]).__name__)
