db.runCommand({'collMod' : 'charm',
   validator: {
$expr : {
 "$or" : [
  { "$and" : [
   { "title" : {"en" : { bsonType: "string" } } },
   { "subtitle" : {"en" : { bsonType: "string" } } },
  ]},
  { "$and" : [
   { "title" : {"en-gb" : { bsonType: "string" } } },
   { "subtitle" : {"en-gb" : { bsonType: "string" } } },
  ]},
 ]},
$jsonSchema: {
      bsonType: "object",
      additionalProperties: false,
      required: [ "aux_charm_flag", "charm_id", "charm_like_count", "charm_save_count", "charm_touch_count", "charm_view_count", "chsketch_id", "created_on", "live_status", "look", "modified_on", "objects_in_charm", "subtitle", "tagger_object", "title", ],
      properties: {
 "_id": {},
  "actor_id": { bsonType: ["int", "double"], },
   "associated_videos": {
  bsonType: "array",
  items: {
   bsonType: ["object"],
   required: [ "video_url", "video_id", "video_type", "video_category", "video_year", "video_name",],
   additionalProperties: false,
   properties: {
    "band_name": { bsonType: "string", },
    "cueid": { bsonType: "number", },
    "movie_name": { bsonType: "string", },
    "video_url": { bsonType: "string", },
    "song_name": { bsonType: "string", },
    "video_id": { bsonType: "string", },
    "video_type": { enum: [ "Movie", "Television/Serial", "Ads", "Web Series" ], },
    "video_category": { enum: [ "Trailer", "Song", "Episode", "Ads", "Web Series" ], },
    "video_year": { bsonType: ["int", "double"], },
    "video_name": { bsonType: "string", },
   }, },},
   "aux_charm_flag": { enum: [ NumberInt(0), NumberInt(1) ], },
 "board_obj": {
  bsonType: "array",
  items: {
   bsonType: ["object"],
   required: ["board_id", "video_id"],
   additionalProperties: false,
   properties: {
    "board_id": { bsonType: ["int"], },
    "video_id": { bsonType: "string", },
   }, },},
 "character_id": { bsonType: ["int","double"], },
   "charm_id": { bsonType: "int", },
   "charm_like_count": { bsonType: "int", },
   "charm_save_count": { bsonType: "int", },
    "charm_touch_count": { bsonType: "int", },
   "charm_view_count": { bsonType: "int", },
   "chsketch_id": { bsonType: ["int","double"], },
  "content_image_cards": {
  bsonType: "array",
  items: {
   bsonType: ["object"],
   additionalProperties: false,
   properties: {
    "card_color": { bsonType: "string", },
    "card_id": { bsonType: ["int", "string", "double"], },
    "dis_card_type": { bsonType: "string", },
    "card_rank": { bsonType: ["int", "string", "double"], },
    "cb_category": { enum: [ "outfit", "lingerie", "beauty", "accessories", "hair", "Outfit", "Lingerie", "Beauty", "Accessories", "Hair", "-1" ], },
    "sub_category": { bsonType: "string", },
    "subsub_category": { bsonType: "string", },
    "text": { bsonType: "string", },
   }, },},
   "created_on": { bsonType: ["long", "double"], },
 "ext_likes": { bsonType: "array", },
 "fscore" : { bsonType: ["int","double"] },
 "isGif": { enum: [ NumberInt(0), NumberInt(1) ], },
  "lang": { bsonType: "array", },
   "lc_bounding_box": { bsonType: "string", },
   "live_status": { enum: [ NumberInt(0), NumberInt(1) ], },
   "look": { enum: [ NumberInt(0), NumberInt(1) ], },
   "modified_on": { bsonType: ["long", "double"], },
   "objects_in_charm": { bsonType: "array", },
  "production_name" : { bsonType: "string" },
 "qscore" : { bsonType: ["int","double"]  },
 "ref_cards" : {bsonType: "array",
  items: {
   bsonType: ["object"],
   additionalProperties: false,
   properties: {
    "card_id": { bsonType: ["int","double"], },
    "category": { bsonType: "string", },
    "product_url": { bsonType: "string", },
    "title": { bsonType: "string", },
    "type": { bsonType: "string", },
   }, },},
 "search_desc": { bsonType: "array", },
 "search_keywords": { bsonType: "array", },
   "subtitle": {
   bsonType: "object",
   properties: {
    "en": { bsonType: "string", },
    "en-gb": { bsonType: "string", },
   }, },
   "tagger_object": { bsonType: "string", },
   "title": {
   bsonType: "object",
   properties: {
    "en": { bsonType: "string", },
    "en-gb": { bsonType: "string", },
   }, },
   "video_id": { bsonType: "string", },
      }
   } },
"validationLevel" : "strict",
"validationAction" : "error"
} );
