from pymongo import MongoClient
from bson.objectid import ObjectId

#uri = "mongodb://tagosprod:tagosprod#132@ds119295-a1.mlab.com:19295/charmboard"
#uri = "mongodb://cbappdev:cbdevap#132@cb-app-0.charmd.me:19295/charmboard_dev"
uri = "mongodb://cbappprod:cbapprod#132@cb-app-2.charmd.me:19295/charmboard"
#uri = "mongodb://139.59.83.215:27017/charmboard_20180607" #change
client = MongoClient(uri)
db = client.get_database() #change
#db = client.charmboard_20180607
#coll = db.video_noval
coll = db.video

required_list = [ "actor_tags", "aux_bounding_box", "character_tags", "cids", "content_type", "created_on", "cueids", "lang", "live_status", "objects", "plgdata", "video_charms", "video_details", "video_dimension", "video_id", "video_info", "video_type", "video_url",  ]

allowed_list = ["_id", "actor_tags",  "aux_bounding_box",  "band_name",  "bbox",  "board_id",  "categories",  "character_tags",  "cids",  "content_type",  "created_on",  "cueids",  "fps",  "geo_location",  "lang",  "live_status",  "modified_on",  "no_of_plays",  "no_of_plays_globally",  "objects",  "plgdata",  "release_date",  "share_type",  "video_charms",  "video_details",  "video_dimension",  "video_global_info",  "video_id",  "video_info",  "video_rank",  "video_type",  "video_url",]

for video in coll.find(): #{"video_id" : "cbve1lPRMG4QJ"}): #{ "_id" : { "$gt" : ObjectId("5b12b47e1f6fda4376a7078f") }}):
	keys_list = video.keys()
	for req_field in required_list:
		if req_field not in keys_list:
			try:
				print(video['video_id'],req_field,"missing")
			except KeyError:
				print("video_id is missing: ",video)
				break
	for key in keys_list:
		if key not in allowed_list:
			try:
				print(video['video_id'],key,"is not allowed")
			except KeyError:
				print("video_id is missing: ",video)
		else:
			if key == "_id":
				continue
			elif key in ["aux_bounding_box", "band_name", "bbox", "categories", "video_id", "video_details", "video_dimension", "video_url"]:
				if type(video[key]).__name__ != "str":
					print(video['video_id'],type(video[key]).__name__,key,"datatype mismatch")
			elif key in ["content_type", "no_of_plays", "no_of_plays_globally"]:
				if type(video[key]).__name__ != 'int':
					print(video['video_id'],type(video[key]).__name__,key,"datatype mismatch")
			elif key in ["live_status", ]:
				if type(video[key]).__name__ != 'int':
					print(video['video_id'],type(video[key]).__name__,key,"datatype mismatch")
				elif video[key] not in [0,1]:
					print(video['video_id'],type(video[key]).__name__,key,"unacceptable value")
			elif key in ["created_on", "modified_on", ]:
				if type(video[key]).__name__ not in ["Int64", "float"]:
					print(video['video_id'],type(video[key]).__name__,key,"datatype mismatch")
			elif key in ["board_id", "release_date", "video_rank"]:
				if type(video[key]).__name__ not in ["int", "float", "Int64"]:
					print(video['video_id'],type(video[key]).__name__,key,"datatype mismatch")
			elif key in ["geo_location"]:
				if type(video[key]).__name__ != "list":
					print(video['video_id'],type(video[key]).__name__,key,"datatype mismatch")
			elif key == "fps":
				if video[key] not in ([ "24", "25", "30", "29", "", "23.98", "1", "60", "26" ]):
					print(video['video_id'],key,video[key],"incorrect value")
			elif key == "share_type":
				if video[key] not in ([ "default", ""]):
					print(video['video_id'],key,video[key],"incorrect value")
			elif key == "video_type":
				if video[key] not in ([ "Song", "Trailer", "Episode", "Web Series", "Ads", "Talk Show", "Song - Movie", "Song - Single", "Song - Album", "News","Food","Technology","Sports","Short Film","Travel","Brand Photo Shoot","Magazine Video", "Event", "Fitness", "Others", "Tutorial", ]):
					print(video['video_id'],key,video[key],"incorrect value")
			elif key in ['actor_tags', "character_tags", "lang", "objects" ]:
				items = video[key]
				for item in items:
					if type(item).__name__ != 'str':
						print(video['video_id'],key,inner_key,type(item).__name__,"datatype mismatch")
			elif key in ['video_charms' ]:
				items = video[key]
				for item in items:
					if type(item).__name__ != 'int':
						print(video['video_id'],key,type(item).__name__,"datatype mismatch")
			elif key in ['cueids' ]:
				items = video[key]
				for item in items:
					if type(item).__name__ != 'float':
						print(video['video_id'],key,inner_key,type(item).__name__,"datatype mismatch")
			elif key == 'cids':
				items = video[key]
				for item in items:
					try:
						inner_keys = item.keys()
						for inner_key in inner_keys:
							if inner_key not in ['cid']:
								print(video['video_id'],key,repr(inner_key),"not allowed")
							elif type(item[inner_key]).__name__ != 'str':
								print(video['video_id'],key,inner_key,type(item[inner_key]).__name__,"datatype mismatch")
					except AttributeError:
						print(video['video_id'],key,video[key])
			elif key in ['plgdata',]:
				inner_keys = video[key].keys()
				for inner_key in inner_keys:
					if inner_key not in ['co','cc','tp','sf','vt','tc']:
						print(video['video_id'],key,repr(inner_key),"not allowed")
					elif type(video[key][inner_key]).__name__ != 'str':
						print(video['video_id'],key,inner_key,type(video[key][inner_key]).__name__,"datatype mismatch")
			elif key in ['video_global_info',]:
				inner_keys = video[key].keys()
				for inner_key in inner_keys:
					if inner_key not in ['channel','channel_id','platform','published_date','views']:
						print(video['video_id'],key,repr(inner_key),"not allowed")
					elif inner_key == 'views':
						if type(video[key][inner_key]).__name__ not in ['Int64','int']:
							print(video['video_id'],key,inner_key,type(video[key][inner_key]).__name__,"datatype mismatch")
					elif inner_key == 'published_date':
						if type(video[key][inner_key]).__name__ not in ['Int64','float','int']:
							print(video['video_id'],key,inner_key,type(video[key][inner_key]).__name__,"datatype mismatch")
					elif type(video[key][inner_key]).__name__ != 'str':
						print(video['video_id'],key,inner_key,type(video[key][inner_key]).__name__,"datatype mismatch")
			elif key in ['video_info',]:
				inner_keys = video[key].keys()
				for inner_key in inner_keys:
					if inner_key not in ['en','en-gb']:
						print(video['video_id'],key,repr(inner_key),"not allowed")
					else:
						inner_inner_keys = video[key][inner_key].keys()
						for inner_inner_key in inner_inner_keys:
							if inner_inner_key not in ['movie_name','song_name','video_name','video_year']:
								print(video['video_id'],key,inner_key,repr(inner_inner_key),"not allowed")
							elif inner_inner_key == 'video_year':
								if type(video[key][inner_key][inner_inner_key]).__name__ not in ['float','int']:
									print(video['video_id'],key,inner_key,inner_inner_key,type(video[key][inner_key][inner_inner_key]).__name__,"datatype mismatch")
							elif type(video[key][inner_key][inner_inner_key]).__name__ != 'str':
								print(video['video_id'],key,inner_key,inner_inner_key,type(video[key][inner_key][inner_inner_key]).__name__,"datatype mismatch")
			else:
				print(video['video_id'],key,type(video[key]).__name__)
